export const environment = {
  firebaseConfig: {
    apiKey: 'AIzaSyAKpIZs50cx6gCqEXGUWX3ls00_QmHpIa4',
    authDomain: 'cuantorentamiafp.firebaseapp.com',
    databaseURL: 'https://cuantorentamiafp.firebaseio.com',
    projectId: 'cuantorentamiafp',
    storageBucket: 'cuantorentamiafp.appspot.com',
    messagingSenderId: '700731943150',
    appId: '1:700731943150:web:aed62033ef5b74ad572bff',
    measurementId: 'G-L8LS62VNZP'
  },
  production: true,
  recaptchaV3ClientKey: '6LdFcbYZAAAAANVyKIyLbV2ns7LhX8QaC6qAb94F',
  apiURL: 'https://us-central1-cuantorentamiafp.cloudfunctions.net/app',
  // apiURL: 'http://localhost:3000',
  endpoints: {
    calculate: '/calculate'
  }
};
