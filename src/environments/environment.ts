// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  firebaseConfig: {
    apiKey: 'AIzaSyAKpIZs50cx6gCqEXGUWX3ls00_QmHpIa4',
    authDomain: 'cuantorentamiafp.firebaseapp.com',
    databaseURL: 'https://cuantorentamiafp.firebaseio.com',
    projectId: 'cuantorentamiafp',
    storageBucket: 'cuantorentamiafp.appspot.com',
    messagingSenderId: '700731943150',
    appId: '1:700731943150:web:aed62033ef5b74ad572bff',
    measurementId: 'G-L8LS62VNZP'
  },
  production: false,
  recaptchaV3ClientKey: '6LdFcbYZAAAAANVyKIyLbV2ns7LhX8QaC6qAb94F',
  apiURL: 'https://us-central1-cuantorentamiafp.cloudfunctions.net/app',
  // apiURL: 'http://localhost:3000',
  endpoints: {
    calculate: '/calculate'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
