import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AfpService {

  calculate(formData: FormData, token: string): Observable<HttpResponse<any>> {
    const headers = new HttpHeaders({
      Authorization: 'Bearer ' + token
    });
    return this.httpClient.post<any>(environment.apiURL + environment.endpoints.calculate, formData, {headers, observe: 'response' });
  }

  constructor(private httpClient: HttpClient) { }
}
