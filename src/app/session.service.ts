import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DialogOverviewComponent } from './dialog-overview/dialog-overview.component';

export interface CalculateResponse {
  sumaCotizaciones: string;
  sumaCotizacionesIPC: string;
  rentabilidad?: string;
  rentabilidadIPC?: string;
  porcentaje?: string;
  porcentajeIPC?: string;
}

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  calculateResponse: CalculateResponse;
  cert: ArrayBuffer | string;

  constructor(public dialog: MatDialog) { }

  openDialog(messages: string): void {
    const dialogRef = this.dialog.open(DialogOverviewComponent, {
      width: '250px',
      data: { message: messages }
    });
    dialogRef.afterClosed().subscribe(result => {
    });
  }
}
