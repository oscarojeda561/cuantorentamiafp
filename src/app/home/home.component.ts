import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AfpService } from '../afp.service';
import { SessionService, CalculateResponse } from '../session.service';
import { ReCaptchaV3Service } from 'ng-recaptcha';
import { HttpResponse } from '@angular/common/http';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  isLoading = false;
  formGroup: FormGroup;
  stepperFormGroup: FormGroup;
  responseData: CalculateResponse;
  showRentabilidad = false;
  fileReader: FileReader;
  fileName: string;

  @ViewChild('continue', { static: false })
  continue: ElementRef;

  constructor(private service: AfpService,
              private session: SessionService,
              private recaptchaV3Service: ReCaptchaV3Service) { }

  ngOnInit(): void {
    this.fileReader = this.getFileReader();
    this.formGroup = new FormGroup({
      historico: new FormControl('', [Validators.required]),
      afp: new FormControl('', Validators.required),
      saldo: new FormControl(''),
      retiroCOVID: new FormControl('')
    });
    this.stepperFormGroup = new FormGroup({
      resultsReady: new FormControl('', Validators.required)
    });
    this.fileReader.onloadend = (e => {
      this.session.cert = this.fileReader.result;
    });
  }

  onSelectedFilesChanged(event: FileList) {
    if (event && event.length > 0 && event[0].type === 'application/pdf' && event[0].size < (10 * 1024 * 1024)) {
      this.fileReader.readAsDataURL(event[0]);
      this.formGroup.get('historico').setValue(event[0]);
      this.fileName = event[0].name;
    } else {
      this.fileName = '';
      this.session.cert = null;
      this.formGroup.get('historico').setValue(null);
    }
  }

  submit() {
    eval("firebase.analytics().logEvent('calculation_requested');");
    this.showRentabilidad = true;
    this.isLoading = true;
    this.recaptchaV3Service.execute('calculate').subscribe((token) => {
      const formData = new FormData();
      const file = new File([this.session.cert], this.fileName);
      formData.append('historico', this.formGroup.get('historico').value);
      formData.append('afp', this.formGroup.get('afp').value);
      formData.append('saldoActual', this.formGroup.get('saldo').value);
      formData.append('retiroCOVID', this.formGroup.get('retiroCOVID').value);
      this.service.calculate(formData, token).subscribe((data: HttpResponse<CalculateResponse>) => {
        this.session.calculateResponse = { ...data.body};
        this.responseData = { ...data.body};
        if (!this.responseData.sumaCotizaciones || this.responseData.sumaCotizaciones === '0') {
          this.session.openDialog('Ocurrió un error inesperado. Verifique que seleccionó correctamente su AFP');
          this.isLoading = false;
          return;
        }
        if (this.responseData.rentabilidad === null) {
          this.showRentabilidad = false;
          this.responseData.rentabilidad = 'Saldo no informado';
          this.responseData.porcentaje = 'Saldo no informado';
          this.responseData.rentabilidadIPC = 'Saldo no informado';
          this.responseData.porcentajeIPC = 'Saldo no informado';
        }
        this.isLoading = false;
        this.stepperFormGroup.get('resultsReady').setValue(true);
        this.continue.nativeElement.click();
      }, (error: any) => {
        this.isLoading = false;
        this.session.openDialog('Ocurrió un error inesperado. Verifique que seleccionó correctamente su AFP');
      });
    }, (error: any) => {
      this.session.openDialog('Ocurrió un error inesperado');
      this.isLoading = false;
    });

  }

  reset() {
    this.formGroup.reset();
    const removeFileButton: HTMLElement = document.querySelector('#cdk-step-content-0-0 > div > form > p:nth-child(2) > mat-file-upload > button.mat-focus-indicator.mat-icon-button.mat-button-base.ng-star-inserted');
    if (removeFileButton) {
      removeFileButton.click();
    }
    this.session.cert = null;
    this.fileName = '';
  }

  getFileReader(): FileReader {
    const fileReader = new FileReader();
    const zoneOriginalInstance = (fileReader as any)['__zone_symbol__originalInstance'];
    return zoneOriginalInstance || fileReader;
}

}
