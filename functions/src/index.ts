import * as functions from 'firebase-functions';
import * as express from 'express';
import * as cors from 'cors';
import { bufferize, getUfValues, Cotizaciones, calcValueWithIPC } from './util/util';
import { calculateHabitat } from './util/habitat';
import { calculateModelo } from './util/modelo';
import { calculateCuprum } from './util/cuprum';
import { calculateProvida } from './util/provida';
import { fileHandler } from './util/middleware';
import { verifyRecaptcha } from './util/recaptcha';
import * as admin from 'firebase-admin';
import * as https from 'https'
import { calculateCapital } from './util/capital';

const pdf2table = require('pdf2table');

admin.initializeApp();
const app = express();
app.use(cors({ origin: true }));
const db = admin.firestore();


app.post('/calculate', verifyRecaptcha, fileHandler, async (req: any, res: any) => {
// app.post('/calculate', fileHandler, async (req: any, res: any) => {
    if (req.files.historico.size > 2097152) {
        res.status(500).send({ error: 'File too large' })
    }
    if (!req.files.historico.fileName.toLowerCase().endsWith('.pdf')) {
        res.status(500).send({ error: 'Extension not supported' })
    }
    let cotizaciones: Cotizaciones;
    const buffer = await bufferize(req.files.historico.buffer);
    const ufValues = await getUfValues(db);
    let saldoActual = 0;
    if (req.body.retiroCOVID) {
        saldoActual = Number.parseInt(req.body.saldoActual) + calcValueWithIPC('2020-08-01', Number.parseInt(req.body.retiroCOVID), ufValues);
    } else {
        saldoActual = Number.parseInt(req.body.saldoActual);
    }
    pdf2table.parse(buffer, function (error: any, rows: any, rowsdebug: any) {
        if (error || !rows) {
            console.log(error);
            res.status(500).send({
                errorCode: 500
            })
        }

        if (req.body.afp === 'Habitat') {
            cotizaciones = calculateHabitat(rows, ufValues)
        }
        else if (req.body.afp === 'Modelo') {
            cotizaciones = calculateModelo(rows, ufValues)
        }
        else if (req.body.afp === 'Cuprum') {
            cotizaciones = calculateCuprum(rows, ufValues)
        }
        else if (req.body.afp === 'Provida') {
            cotizaciones = calculateProvida(rows, ufValues)
        }
        else if (req.body.afp === 'Capital') {
            cotizaciones = calculateCapital(rows, ufValues)
        }
        else {
            res.status(500).send({ error: 'AFP not found' })
        }

        const sumaCotizaciones = cotizaciones.sumaCotizaciones;
        let sumaCotizacionesIPC = cotizaciones.sumaCotizacionesIPC
        const rentabilidad = saldoActual - cotizaciones.sumaCotizaciones;
        let rentabilidadIPC = saldoActual - cotizaciones.sumaCotizacionesIPC;
        const porcentaje = Math.round((rentabilidad / cotizaciones.sumaCotizaciones) * 10000) / 100;
        const porcentajeIPC = Math.round((rentabilidadIPC / cotizaciones.sumaCotizacionesIPC) * 10000) / 100;
        const percentValue = porcentaje + '%'
        const percentValueIPC = porcentajeIPC + '%'

        sumaCotizacionesIPC = Number.parseInt(sumaCotizacionesIPC.toFixed());
        rentabilidadIPC = Number.parseInt(rentabilidadIPC.toFixed());
        db.collection('calculations').doc(new Date().getTime().toString()).set({sumaCotizaciones, sumaCotizacionesIPC, rentabilidad,
            rentabilidadIPC, porcentaje: percentValue, porcentajeIPC: percentValueIPC, date: new Date()}).then().catch(err => {console.log(err)});
        res.send({ sumaCotizaciones, sumaCotizacionesIPC, rentabilidad, rentabilidadIPC, porcentaje: percentValue, porcentajeIPC: percentValueIPC })
    });
});

app.post('/updateUF', async (req: any, res: any) => {
    const body = JSON.parse(req.body)
    const secret = functions.config().recaptcha.secret
    if (body.secret === secret) {
        try {
            const year = new Date().getUTCFullYear().toString();
            const options = {
                hostname: 'mindicador.cl',
                path: `/api/uf/${year}`,
                method: 'GET',
            };
            const request = https.request(options, (resp: any) => {
                let data = '';
                resp.on('data', (chunk: any) => {
                    data += chunk;
                });
                resp.on('end', async () => {
                    const responseBody = JSON.parse(data);
                    if (responseBody && responseBody.serie) {
                        await db.collection('valorUF').doc(year).set(
                            {
                                valores: responseBody.serie
                            }
                        );
                        res.status(200).send({ result: 'ok' })
                    };
                });
            }).on('error', (error) => {
                res.status(500).send({
                    code: 'error/unknown',
                    message: error.message
                });
            });
            request.end();
        } catch (error) {
            res.status(500).send(error);
        };
    } else res.status(401).send({ error: 'not allowed' })

});

exports.app = functions.https.onRequest(app);

// app.listen(3000, () => console.log('Example app listening at http://localhost:3000'))
