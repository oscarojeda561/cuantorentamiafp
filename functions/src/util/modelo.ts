import { calcValueWithIPC } from "./util";

export function calculateModelo (rows: any, ufValues: []) {
    let sumaCotizaciones = 0;
    let sumaCotizacionesIPC = 0;
    rows.forEach((element: any) => {
        if (element[1] &&
            (element[1].includes('COTIZACION')
                || element[1].includes('RELIQUIDACION')
                || element[1].includes('COT. NORMAL'))) {
            const paySanitized = element[3].replace('.', '');
            sumaCotizaciones += Number.parseFloat(paySanitized);
            sumaCotizacionesIPC += calcValueWithIPC(getFechaFormatted(element[2]),
        Number.parseFloat(paySanitized), ufValues);
        }
        else if (element.length === 21 &&
            (element[2].includes('COTIZACION')
                || element[2].includes('RELIQUIDACION')
                || element[2].includes('COT. NORMAL'))) {
            const paySanitized = element[8].replace('.', '');
            sumaCotizaciones += Number.parseFloat(paySanitized);
            sumaCotizacionesIPC += calcValueWithIPC(getFechaFormatted(element[4]),
        Number.parseFloat(paySanitized), ufValues);
        }
    });
    return {sumaCotizaciones, sumaCotizacionesIPC};
}

function getFechaFormatted(fecha: string) {
    const fechaOldSplited = fecha.split("/");
    const fechaOldFormatted = fechaOldSplited[2] + "-" + fechaOldSplited[1] + "-" + fechaOldSplited[0];
    return fechaOldFormatted;
  }