import { calcValueWithIPC } from "./util";

export function calculateCapital(rows: any, ufValues: []) {
    let sumaCotizaciones = 0;
    let sumaCotizacionesIPC = 0;
    rows.forEach((element: any) => {
        if (element[2] &&
            (element[2] === 'COTIZACIÓN')) {
            let paySanitized = element[4].replace('.', '');
            paySanitized = paySanitized.replace('$', '');
            sumaCotizaciones += Number.parseFloat(paySanitized);
            sumaCotizacionesIPC += calcValueWithIPC(getFechaFormatted(element[3]),
                Number.parseFloat(paySanitized), ufValues);
        }
    });
    return { sumaCotizaciones, sumaCotizacionesIPC };
}

function getFechaFormatted(fecha: string) {
    const fechaOldSplited = fecha.split("/");
    const fechaOldFormatted = fechaOldSplited[2] + "-" + fechaOldSplited[1] + "-" + fechaOldSplited[0];
    return fechaOldFormatted;
}