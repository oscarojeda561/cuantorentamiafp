import { calcValueWithIPC } from "./util";

export function calculateProvida(rows: any, ufValues: []) {
    let sumaCotizaciones = 0;
    let sumaCotizacionesIPC = 0;
    rows.forEach((element: any) => {
        if (element.length === 6 &&
            element[2].includes('$')) {
            let paySanitized = element[2].replace('$', '');
            paySanitized = paySanitized.replace('.', '');
            sumaCotizaciones += Number.parseFloat(paySanitized);
            sumaCotizacionesIPC += calcValueWithIPC(element[5],
                Number.parseFloat(paySanitized), ufValues);
                
        }
    });
    return {sumaCotizaciones, sumaCotizacionesIPC};
}