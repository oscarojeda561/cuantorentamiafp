import { calcValueWithIPC } from "./util";

export function calculateCuprum(rows: any, ufValues: []) {
  let sumaCotizaciones = 0;
  let sumaCotizacionesIPC = 0;
  rows.forEach((element: any) => {
    if (element[1] &&
      (element[1] === 'COTIZACION')) {
      const paySanitized = element[3].replace('.', '');
      sumaCotizaciones += Number.parseFloat(paySanitized);
      sumaCotizacionesIPC += calcValueWithIPC(getFechaFormatted(element[2]),
        Number.parseFloat(paySanitized), ufValues);
    }
  });
  return {sumaCotizaciones, sumaCotizacionesIPC};
}

function getFechaFormatted(fecha: string) {
  const fechaOldSplited = fecha.split("/");
  const fechaOldFormatted = fechaOldSplited[2] + "-" + fechaOldSplited[1] + "-" + fechaOldSplited[0];
  return fechaOldFormatted;
}