import * as functions from 'firebase-functions';
import * as https from 'https';
import * as querystring from 'querystring';

export const verifyRecaptcha = (req: any, res: any, next: any) => {
    if (!req.headers.authorization || !req.headers.authorization.startsWith('Bearer ')) {
        return res.status(401).send({
            code: 'auth/unauthenticated',
            message: 'a valid reCAPTCHA token must be provided'
        });
    };
    const token = req.headers.authorization.split('Bearer ')[1];
    const postData = querystring.stringify({
        'secret': functions.config().recaptcha.secret,
        'response': token
    });
    const options = {
        hostname: 'www.google.com',
        path: '/recaptcha/api/siteverify',
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Content-Length': Buffer.byteLength(postData)
        }
    };
    const request = https.request(options, (response) => {
        let data = '';
        response.on('data', (chunk) => {
            data += chunk;
        });
        response.on('end', () => {
            const body = JSON.parse(data);
            if (body.success && body.score >= 0.4) next()
            else {
                res.status(403).send({
                    code: 'auth/bot-detected',
                    message: 'bots are not allowed on this site'
                });
            };
        });
    })
    .on('error', (error) => {
        res.status(500).send({
            code: 'error/unknown',
            message: error.message
        });
    });
    request.write(postData);
    request.end();
};