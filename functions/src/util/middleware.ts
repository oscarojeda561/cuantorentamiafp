const Busboy = require('busboy');

const allowedMethods: string[] = ['POST', 'PUT'];

export class FileUpload {

    public fileName: string;
    public encoding: string;
    public mimeType: string;
    protected buffer: Buffer;

    constructor(opts: any) {
        this.fileName = opts.fileName;
        this.encoding = opts.encoding;
        this.mimeType = opts.mimeType;
        this.buffer = new Buffer('');
    }

    public appendData(data: any) {
        this.buffer = Buffer.concat([this.buffer, data]);
    }

    public getBuffer(): Buffer {
        return this.buffer;
    }

    public getBytes(): number {
        return this.buffer.byteLength;
    }

}

export const fileHandler = function (req: any, res: any, next: any) {
    if (
        allowedMethods.includes(req.method) && req.rawBody &&
        req.headers['content-type'].startsWith('multipart/form-data')
    ) {
        const busboy = new Busboy({ headers: req.headers });
        const files: { [fieldName: string]: FileUpload } = {};
        req.body = {};
        busboy.on('file', (fieldName: any, file: any, fileName: any, encoding: any, mimeType: any) => {
            files[fieldName] = new FileUpload({
                fileName: fileName,
                encoding: encoding,
                mimeType: mimeType
            });
            file.on('data', (data: any) => {
                files[fieldName].appendData(data);
            });
        });
        busboy.on('field', (fieldName: any, value: any) => {
            req.body[fieldName] = value;
        });
        busboy.on('finish', () => {
            req.files = files;
            next();
        });
        busboy.end(req.rawBody);
    } else {
        next();
    }
};
