import { Firestore } from '@google-cloud/firestore';

export interface UFValueDay {
  fecha: string;
  valor: string;
}

export interface Cotizaciones {
  sumaCotizaciones: number;
  sumaCotizacionesIPC: number
}

export function bufferize(file: any): Promise<any> {
  return new Promise(function (resolve, reject) {
    const pdf = Buffer.from(file)
    resolve(pdf)
  });
}

export const getUfValues = async (db: Firestore): Promise<any> => {
  //date: formato año-mes-dia
  const collection = await db.collection('valorUF').get();
  const allResults = collection.docs.map((doc: any) => doc.data().valores);
  let newArr: UFValueDay[] = [];
  for (const iterator of allResults) {
    newArr = newArr.concat(iterator);
  }
  return newArr;
}

export const calcValueWithIPC = (oldDate: string, cot: number, ufArray: Array<UFValueDay>) => {
  let ufValueToday = 0;
  let ufValueOld = 0;
  const today = new Date();
  const yearNow = today.getUTCFullYear().toString();
  let month = (today.getMonth() + 1).toString();
  let day = today.getDate().toString();
  if (month.length === 1) {
    month = "0" + month;
  }
  if (day.length === 1) {
    day = "0" + day;
  }
  const todayDateFormatted = yearNow + "-" + month + "-" + day;
  for (const iterator of ufArray) {
    if (iterator.fecha.startsWith(oldDate)) {
      ufValueOld = Number.parseFloat(iterator.valor);
    }
    else if (iterator.fecha.startsWith(todayDateFormatted)) {
      ufValueToday = Number.parseFloat(iterator.valor);
    }
  }
   if (ufValueOld === 0) {
    ufValueOld = ufValueToday;
  }
  return ((cot / ufValueOld) * ufValueToday);
}
