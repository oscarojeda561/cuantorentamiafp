import { calcValueWithIPC, Cotizaciones } from "./util";

export function calculateHabitat(rows: any, ufValues: []): Cotizaciones {
  let sumaCotizaciones = 0;
  let sumaCotizacionesIPC = 0;
  rows.forEach((element: any) => {
    if (element[1] &&
      (element[1] === 'Cotizacion' || element[1] === 'como rezago anterior traspasada'
        || element[1] === 'independiente por transferencia TGR'
        || element[1].includes('como rezago descoordinado'))) {
      let paySanitized = element[3].replace('$', '');
      paySanitized = paySanitized.replace('.', '');
      sumaCotizaciones += Number.parseFloat(paySanitized);
      sumaCotizacionesIPC += calcValueWithIPC(getFechaFormatted(element[2]),
        Number.parseFloat(paySanitized), ufValues);
    }
    else if (element[1] === 'Pago Electronico Cotizacion' && element[2] === 'Obligatoria') {
      let paySanitized = element[4].replace('$', '');
      paySanitized = paySanitized.replace('.', '');
      sumaCotizaciones += Number.parseFloat(paySanitized);
      sumaCotizacionesIPC += calcValueWithIPC(getFechaFormatted(element[3]),
        Number.parseFloat(paySanitized), ufValues);
    }
  });
  return {sumaCotizaciones, sumaCotizacionesIPC};
}

function getFechaFormatted(fecha: string) {
  const fechaOldSplited = fecha.split("/");
  const fechaOldFormatted = fechaOldSplited[2] + "-" + fechaOldSplited[1] + "-" + fechaOldSplited[0];
  return fechaOldFormatted;
}