import { assert } from 'chai'
import { calcValueWithIPC, UFValueDay } from '../src/util/util'
describe("Calculate COVID redrawal with IPC ", function () {
  it("It should be gratter than 0", function () {
    const ufArray: Array<UFValueDay> = [
      {
        fecha: '2021-04-09T04:00:00.000Z',
        valor: '29411.83'
      },
      {
        fecha: '2021-04-08T04:00:00.000Z',
        valor: '29409.93'
      },
      {
        fecha: '2021-04-07T04:00:00.000Z',
        valor: '29408.04'
      },
      {
        fecha: '2020-08-01T04:00:00.000Z',
        valor: '28666.51'
      }
    ]

    const today = new Date();
    const yearNow = today.getUTCFullYear().toString();
    let month = (today.getMonth() + 1).toString();
    let day = today.getDate().toString();
    if (month.length === 1) {
      month = "0" + month;
    }
    if (day.length === 1) {
      day = "0" + day;
    }
    const todayDateFormatted = yearNow + "-" + month + "-" + day;
    ufArray.push({fecha: todayDateFormatted, valor: '30000'})
    const result = calcValueWithIPC('2020-08-01', Number.parseInt('1000000'), ufArray);
    assert.notEqual(result, 0);
    assert.notEqual(result, NaN);
  });
});
