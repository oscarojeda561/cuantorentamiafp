# Cuantorentamiafp (https://cuantorentamiafp.cl)

Este proyecto fue creado para calcular la rentabilidad histórica de las cuentas obligatorias personales de las AFP chilenas. La aplicación realiza los cálculos con el archivo PDF de las cotizaciones históricas de su AFP, y toma en cuenta el factor inflación a partir de los valores del indicador UF, el cual se actualiza todos los días en la base de datos con una llamada programada a la API del sitio https://mindicador.cl/

# Stack

Plataforma: Firebase + Google Cloud Platform

Servicios: Firebase Hosting, Firebase Functions, Firestore, Scheduler

Frontend: Angular

Backend: Node js + Express js

DB: Firestore 


# Ejecutando el proyecto

El proyecto frontend está desarrollado en angular y se puede ejecutar en local con el comando ```ng serve```.

El backend está desarrollado utilizando Express js, se ubica en la carpeta ```/functions```, y está orientado a ser desplegado como Google Cloud Function. Una vez desplegado se le asigna automáticamente una variable de ambiente la cual tiene la información de la service account (credenciales de autenticación ) para acceder al servicio de Firestore (Base de datos). Por temas de seguridad esta service account no puede ser compartida con terceros, por lo que su ejecución en local no es factible.

Si desea ejecutar las pruebas unitarias lo puede realizar con el comando: ```npm run test```
